import React from 'react'
import { addDecorator } from '@storybook/react'
import { BrowserRouter } from 'react-router-dom'
import { IntlProvider } from 'react-intl';

import './animation.css';

import en from '../src/translations/en.json';
import sv from '../src/translations/sv.json';

const messages = {
  en,
  sv
}

import { getLocale } from '../src/util/localeHelper'

addDecorator(story => <BrowserRouter initialEntries={['/']}>{story()}</BrowserRouter>);
addDecorator(story => <IntlProvider locale={getLocale()} messages={messages[getLocale()]}>{story()}</IntlProvider>)

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
