module.exports = {
  plugins: [
    'stylelint-scss',
  ],
  processors: [
    [ '@mapbox/stylelint-processor-arbitrary-tags', { fileFilterRegex: [ /\.vue$/ ] } ],
  ],
  extends: 'stylelint-config-standard',
  rules: {
    'no-empty-source': null,
    'no-descending-specificity': null,
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': [ true, { ignoreAtRules: [ 'svg-load', 'svg-inline' ] } ],
    'selector-max-id': 0,
    'color-hex-length': 'long',
    'unit-no-unknown': null,
    'value-keyword-case': null,
  },
};
