import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { arrayOf, shape, object, bool, func, number, oneOf, string } from 'prop-types';

import { ImageSlider } from '..';
import { DAILY_BOOKING, HOURLY_BOOKING, MONTHLY_BOOKING } from '../../util/types';
import { createSlug } from '../../util/urlHelpers';

import css from './ListingCard.module.scss';

const HeartIcon = ({ isFavorite, onClick }) => (
  <button type="button" className={css.favoriteButton} onClick={onClick}>
    <svg
      className={classNames(css.favoriteIcon, isFavorite && css.active)}
      width="25"
      height="21"
      viewBox="0 0 25 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M22.2265 2.67974C21.6645 2.14721 20.9972 1.72477 20.2628 1.43656C19.5284 1.14834 18.7412 1 17.9463 1C17.1513 1 16.3641 1.14834 15.6297 1.43656C14.8953 1.72477 14.228 2.14721 13.666 2.67974L12.4997 3.7844L11.3334 2.67974C10.1982 1.60458 8.65854 1.00056 7.05315 1.00056C5.44775 1.00056 3.90811 1.60458 2.77293 2.67974C1.63774 3.7549 1 5.21313 1 6.73364C1 8.25415 1.63774 9.71238 2.77293 10.7875L3.93926 11.8922L12.4997 20L21.0602 11.8922L22.2265 10.7875C22.7887 10.2553 23.2348 9.62328 23.5391 8.9277C23.8434 8.23212 24 7.48657 24 6.73364C24 5.98071 23.8434 5.23516 23.5391 4.53958C23.2348 3.84399 22.7887 3.21201 22.2265 2.67974Z"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  </button>
);

HeartIcon.defaultProps = {
  onClick: null,
};

HeartIcon.propTypes = {
  isFavorite: bool.isRequired,
  onClick: func,
};

const StarIcon = () => (
  <svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M7.5 0L9.8175 5.08278L15 5.90285L11.25 9.85702L12.135 15.4432L7.5 12.8044L2.865 15.4432L3.75 9.85702L0 5.90285L5.1825 5.08278L7.5 0Z"
      fill="#E8909C"
    />
  </svg>
);

export const BADGE_COLORS = {
  GREY: 'grey',
  GREEN: 'green',
  RED: 'red',
};

BADGE_COLORS.LIST = [ BADGE_COLORS.GREY, BADGE_COLORS.GREEN, BADGE_COLORS.RED ];

const Badge = ({ text, color }) => <div className={classNames(css.badge, css[color])}>{text}</div>;

Badge.defaultProps = {
  color: BADGE_COLORS.GREEN,
};

Badge.propTypes = {
  text: string.isRequired,
  color: oneOf(BADGE_COLORS.LIST),
};

const BOOKING_TYPES = {
  DAILY_BOOKING,
  HOURLY_BOOKING,
  MONTHLY_BOOKING,
};

BOOKING_TYPES.LIST = [ DAILY_BOOKING, HOURLY_BOOKING, MONTHLY_BOOKING ];

const ListingCardNew = ({
  title,
  location,
  type,
  price,
  rating,
  features,
  /* isFavorite, */
  id,
  images,
  badgeText,
  badgeColor,
  unitTranslationKey,
  /* onToggleFavorite, */
}) => (
  <div className={css.listingCard}>
    <Link
      className={css.listingLink}
      name="ListingPage"
      to={`/search/${id}/${createSlug(title)}`}
      params={{
        id, slug: createSlug(title),
      }}
    >
      <ImageSlider
        category="images"
        data={{
          images,
        }}
      />
      <div className={css.content}>
        <div className={css.top}>
          <h3 className={css.title}>{title}</h3>
          <span className={css.location}>{location}</span>
        </div>
        <div className={css.bottom}>
          <p className={css.type}>{type}</p>
          <p className={css.price}>
            <strong>{price.formattedPrice}</strong>
            <FormattedMessage id={unitTranslationKey} />
          </p>
          <p className={css.features}>{features}</p>
          {rating && (
            <div className={css.rating}>
              <StarIcon />
              <span>{rating}</span>
            </div>
          )}
        </div>
      </div>
    </Link>
    {badgeText && <Badge text={badgeText} color={badgeColor} />}
    {/* TODO: Uncomment when the favorite listings will be implemented */}
    {/* <HeartIcon isFavorite={isFavorite} onClick={() => onToggleFavorite(id)} /> */}
  </div>
);

ListingCardNew.defaultProps = {
  rating: null,
  /* isFavorite: false, */
  id: null,
  features: null,
  badgeText: null,
  badgeColor: BADGE_COLORS.GREEN,
  /* onToggleFavorite: null, */
  unitTranslationKey: BOOKING_TYPES.DAILY_BOOKING,
};

ListingCardNew.propTypes = {
  title: string.isRequired,
  location: string.isRequired,
  type: string.isRequired,
  price: number.isRequired,
  /* isFavorite: bool, */
  images: arrayOf(
    shape({
      attributes: {
        variants: object,
      },
    }),
  ).isRequired,
  id: string,
  features: string,
  rating: number,
  badgeText: string,
  badgeColor: oneOf(BADGE_COLORS.LIST),
  /* onToggleFavorite: func, */
  unitTranslationKey: oneOf(BOOKING_TYPES.LIST),
};

export default ListingCardNew;
