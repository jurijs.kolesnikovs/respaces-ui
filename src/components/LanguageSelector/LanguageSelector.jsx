import React from 'react';
import classNames from 'classnames';

import { getLocale, setLocale } from '../../util/localeHelper';
import { LANGUAGE_CODES } from '../../translations/languages';

import css from './LanguageSelector.module.scss';

const LanguageSelector = () => {
  const locale = getLocale();
  const capitalize = str => `${str.charAt(0).toUpperCase()}${str.substr(1)}`;

  return (
    <div className={css.selector}>
      {LANGUAGE_CODES.LIST.map(lang => (
        <button
          type="button"
          onClick={() => setLocale(lang)}
          className={classNames(css.language, locale === lang && css.active)}
        >
          {capitalize(lang)}
        </button>
      ))}
    </div>
  );
};

export default LanguageSelector;
