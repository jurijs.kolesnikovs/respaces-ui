import React from 'react';
import classNames from 'classnames';
import { shape, string, arrayOf } from 'prop-types';

import Button from '../Button/Button';

import { injectIntl, intlShape, FormattedMessage } from '../../util/reactIntl';

import css from './Footer.module.scss';

// Instagram page is used in SEO schema (http://schema.org/Organization)
const siteInstagramPage = 'https://www.instagram.com/respaces.co/';

// Linkedin page is used in SEO schema (http://schema.org/Organization)
const siteLinkedinPage = 'https://www.linkedin.com/company/respaces/';

// Facebook page is used in SEO schema (http://schema.org/Organization)
const siteFacebookPage = 'https://www.facebook.com/Respaces.co/';

const siteSpotifyPage = 'https://open.spotify.com/user/jeanna.lundberg?si=DrYPA4xoQReDwjVTkS4jRQ';

const CURRENT_YEAR = new Date().getFullYear();

const ColumnWithLinks = ({ column: { title, links } }) => (
  <div className="gridItem">
    <h5 className="columnHeader">{title}</h5>
    {links.map(link => (
      link.type === 'internal' ? (
        <Button
          style={{
            lineHeight: '28px',
          }}
          name={link.to}
          type="inline"
          linkColor="white"
          textLight
        >
          {link.text}
        </Button>
      ) : (
        <a className={css.link} href={link.to} target="_blank" rel="noopener noreferrer">
          {link.text}
        </a>
      )
    ))}
  </div>
);

ColumnWithLinks.propTypes = {
  column: shape({
    title: string,
    links: arrayOf({
      text: string,
      type: string,
      to: string,
    }),
  }).isRequired,
};

const Footer = ({ intl }) => {
  const footerLinks = {
    explore: {
      title: intl.formatMessage({
        id: 'Footer.explore',
      }),
      links: [
        {
          text: intl.formatMessage({
            id: 'Footer.workspaceSubscriptions',
          }),
          to: '/?category=subscriptions',
          type: 'internal',
        },
        {
          text: intl.formatMessage({
            id: 'Footer.workspaceOnDemand',
          }),
          to: '/?category=onDemand',
          type: 'internal',
        },
        {
          text: intl.formatMessage({
            id: 'Footer.becomeAHost',
          }),
          to: 'SpaceSupplier',
          type: 'named',
        },
      ],
    },
    company: {
      title: intl.formatMessage({
        id: 'Footer.company',
      }),
      links: [
        {
          text: intl.formatMessage({
            id: 'Footer.termsAndPolicies',
          }),
          to: 'TermsOfServicePage',
          type: 'named',
        },
        {
          text: intl.formatMessage({
            id: 'Footer.careers',
          }),
          to: 'CareersPage',
          type: 'named',
        },
        {
          text: intl.formatMessage({
            id: 'Footer.faq',
          }),
          to: 'FAQPage',
          type: 'named',
        },
        {
          text: intl.formatMessage({
            id: 'Footer.contact',
          }),
          to: 'ContactUs',
          type: 'named',
        },
      ],
    },
    follow: {
      title: intl.formatMessage({
        id: 'Footer.follow',
      }),
      links: [
        {
          text: 'Instagram', to: siteInstagramPage, type: 'external',
        },
        {
          text: 'LinkedIn', to: siteLinkedinPage, type: 'external',
        },
        {
          text: 'Facebook', to: siteFacebookPage, type: 'external',
        },
        {
          text: 'Spotify', to: siteSpotifyPage, type: 'external',
        },
      ],
    },
  };

  const subscribeCtaClassList = classNames(css.gridItem, css.subscribeCta);
  const languageSelectorClassList = classNames(css.gridItem, css.languageSelector);
  const allRightsReservedClassList = classNames(css.gridItem, css.allRightsReserved);

  return (
    <footer className={css.footer}>
      <div className={css.container}>
        <div className={css.grid}>
          <ColumnWithLinks column={footerLinks.explore} />
          <ColumnWithLinks column={footerLinks.company} />
          <ColumnWithLinks column={footerLinks.follow} />
          <div className={subscribeCtaClassList}>
            <FormattedMessage id="Footer.subscribeHeading">
              {id => <h5 className={css.columnHeader}>{id}</h5>}
            </FormattedMessage>
            <FormattedMessage id="Footer.subscribeMessage">
              {id => <p className={css.text}>{id}</p>}
            </FormattedMessage>
            <div className={css.subscribeCtaButtonWrapper}>
              <FormattedMessage id="Footer.subscribeButton">
                {id => (
                  <Button type="white">
                    {id}
                  </Button>
                )}
              </FormattedMessage>
            </div>
          </div>
          <div className={languageSelectorClassList} />
          <div className={allRightsReservedClassList}>
            &copy;
            {' '}
            {CURRENT_YEAR}
            {' '}
            Respaces. All Rights Reserved
          </div>
        </div>
      </div>
    </footer>
  );
};

Footer.propTypes = {
  intl: intlShape.isRequired,
};

export default injectIntl(Footer);
