import React from 'react';
import classNames from 'classnames';
import {
  string,
  func,
  bool,
  arrayOf,
  shape,
} from 'prop-types';

import css from './ContentToggle.module.scss';

const ContentToggle = props => {
  const {
    options,
    name,
    value,
    onChange,
    disabled,
  } = props;

  const tabulatorClassList = classNames(css.tabulator, disabled && css.disabled);

  return (
    <div className={css.tabulatorWrap}>
      <div className={tabulatorClassList}>
        {options.map(option => (
          <label
            key={`${name}_option_${option.id}`}
            htmlFor={`${name}_option_${option.id}`}
            className={`${css.tabulatorButton} ${value === option.id ? css.active : ''}`}
          >
            {option.label}
            <input
              className={css.tabulatorRadio}
              type="radio"
              name={name}
              value={option.id}
              id={`${name}_option_${option.id}`}
              onChange={() => {
                if (!disabled) {
                  onChange(option.id);
                }
              }}
            />
          </label>
        ))}
      </div>
    </div>
  );
};

ContentToggle.defaultProps = {
  disabled: false,
};

ContentToggle.propTypes = {
  options: arrayOf(shape({
    id: string, label: string,
  })).isRequired,
  value: string.isRequired,
  disabled: bool,
  name: string.isRequired,
  onChange: func.isRequired,
};

export default ContentToggle;
