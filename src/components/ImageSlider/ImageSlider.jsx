import React from 'react';
import classNames from 'classnames';
import { array, bool, object, oneOf, oneOfType, string } from 'prop-types';
import SwiperCore, { Autoplay, Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import config from '../../config';

import left from './assets/left.svg';
import right from './assets/right.svg';
import bullet from './assets/bullet.svg';

import css from './ImageSlider.module.scss';

const CATEGORIES = [ 'images', 'spaces', 'reviews', 'text' ];
const DIRECTIONS = [ 'horizontal', 'vertical' ];

SwiperCore.use([ Autoplay, Navigation, Pagination ]);
const ImageSlider = props => {
  /* Slider takes :
  * autoplay(to use default 3 second delay) or autoplay{{delay:5000}}
  * for custom delay - defaults to false
  * category to determine if it is displaying several images of 1 listing,
  * 1 image of each of several listings with text, or a review object(or array of review objects)
  * data is the object/array of listings or images or reviews
  * showLocation and showTitle both bools to show location title and
  * location as on Landing Page. default to false
  * */
  const {
    autoplay,
    category,
    className,
    data,
    direction,
    showLocation,
    showTitle,
    allowTouchMove,
  } = props;

  if (!data) {
    return null;
  }
  const classes = classNames(
    className,
    css.sliderContainer,
    category === 'reviews' ? css.sliderContainerReviews : '',
    category === 'text' ? css.textContainer : '',
  );
  // if category = images, create array of small images for listing card.
  // if category = spaces then the listing/data object will be mapped through directly.
  // if category = reviews then will map through that object directly.
  const images = category === CATEGORIES[0]
    ? data.images.map(img => img.attributes.variants['square-small2x'].url)
    : null;

  // eslint-disable-next-line no-nested-ternary
  const content = category === CATEGORIES[0] && images
    ? images.map(img => (
      <SwiperSlide key={`swiperSlide_images_${img}`}>
        <img className={css.image} src={img} alt="work here" />
      </SwiperSlide>
    ))
    // eslint-disable-next-line no-nested-ternary
    : category === CATEGORIES[1]
      ? data.map(listing => (
        <SwiperSlide key={`swiperSlide_listings_${listing.id.uuid}`}>
          <a
            href={`${config.canonicalRootURL}/l/${listing.id.uuid}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              className={css.spacesImage}
              src={
                listing.images[0]?.attributes?.variants['scaled-medium']?.url
                || listing.images[0]?.attributes?.variants['square-small2x']?.url
              }
              alt="work here"
            />
          </a>
          <div className={css.spacesInfoContainer}>
            {showTitle && <h3 className={css.spacesTitle}>{listing.attributes.title}</h3>}
            {showLocation && (
              <h4 className={css.spacesLocation}>
                {listing.attributes.publicData.location.address.split(', ').slice(-2).join(' ')}
              </h4>
            )}
          </div>
        </SwiperSlide>
      ))
      // eslint-disable-next-line no-nested-ternary
      : category === CATEGORIES[2]
        ? data.map(review => (
          <SwiperSlide key={`swiperSlide_reviews_${review.img}`}>
            <div className={css.reviewsSlideContainer}>
              {review.img && <img className={css.reviewImage} src={review.img} alt="work here" />}
              <div>
                {review.text && <h3>{review.text}</h3>}
                {review.author && <p>{review.author}</p>}
              </div>
            </div>
          </SwiperSlide>
        ))
        : category === CATEGORIES[3]
          ? data.map(text => (
            <SwiperSlide key={`swiperSlide_reviews_${text}`}>
              <div className={css.textSlideContainer}>{text}</div>
            </SwiperSlide>
          ))
          : null;

  return (
    <div className={classes}>
      <Swiper
        className={css['swiper-container']}
        spaceBetween={0}
        slidesPerView={1}
        allowTouchMove={allowTouchMove}
        speed={500}
        loop
        direction={direction}
        initialSlide={0}
        autoplay={
          autoplay && {
            delay: autoplay.delay || 3000,
            disableOnInteraction: false,
          }
        }
        navigation={{
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        }}
        pagination={{
          el: '.swiper-pagination',
          type: 'bullets',
          bulletActiveClass: css['pagination-bullet-active'],
          bulletClass: css['pagination-bullet'],
          renderBullet: (index, paginationClassName) => `<img class="${paginationClassName}" alt="slider button" src=${bullet} />`,
        }}
      >
        {content}
        <div className={`swiper-button-next ${css.swiperButton}`}>
          <img alt="slider button" src={right} />
        </div>
        <div className={`swiper-button-prev ${css.swiperButton}`}>
          <img alt="slider button" src={left} />
        </div>
        <div className={`swiper-pagination ${css.swiperPagination}`} />
      </Swiper>
    </div>
  );
};

ImageSlider.defaultProps = {
  autoplay: false,
  category: CATEGORIES[0],
  className: null,
  data: {
  },
  direction: DIRECTIONS[0],
  showLocation: false,
  showTitle: false,
  allowTouchMove: true,
};
ImageSlider.propTypes = {
  autoplay: oneOfType([ bool, object ]),
  category: oneOf(CATEGORIES),
  className: string,
  data: oneOfType([ array, object ]),
  direction: oneOf(DIRECTIONS),
  showLocation: bool,
  showTitle: bool,
  allowTouchMove: bool,
};

export default ImageSlider;
