import React from 'react';
import classNames from 'classnames';
import { oneOf, bool } from 'prop-types';

import { IconReviewStar } from '..';
import { FILL_COLORS } from '../IconReviewStar/IconReviewStar';

import css from './RatingStars.module.scss';

export const REVIEW_RATINGS = [ 1, 2, 3, 4, 5 ];

export const STAR_SIZES = {
  SMALL: 's',
  LARGE: 'l',
};

STAR_SIZES.LIST = [
  STAR_SIZES.SMALL,
  STAR_SIZES.LARGE,
];

const RatingStars = ({ rating, fillColor, condensed, size }) => (
  <div className={css.starList}>
    {REVIEW_RATINGS.map(star => (
      <span className={classNames(css.star, css[size], condensed && css.condensed)}>
        <IconReviewStar
          key={`star-${star}`}
          fillColor={fillColor}
          isFilled={star <= rating}
        />
      </span>
    ))}
  </div>
);

RatingStars.defaultProps = {
  fillColor: FILL_COLORS.BLACK,
  condensed: true,
  size: STAR_SIZES.LARGE,
};

RatingStars.propTypes = {
  rating: oneOf(REVIEW_RATINGS).isRequired,
  fillColor: oneOf(FILL_COLORS.LIST),
  condensed: bool,
  size: oneOf(STAR_SIZES.LIST),
};

export default RatingStars;
