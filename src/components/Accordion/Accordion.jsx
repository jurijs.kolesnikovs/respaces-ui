import React, { useState } from 'react';
import { shape, arrayOf, string } from 'prop-types';
import { CSSTransition } from 'react-transition-group';

import css from './Accordion.module.scss';

const Accordion = ({ list }) => {
  const [ openSection, setOpenSection ] = useState('');
  return (
    <div>
      {list.length && list.map((item, index) => (
        <div key={item.heading} className={css.wwrapper}>
          <div
            className={css.header}
            onClick={() => (openSection === index ? setOpenSection('') : setOpenSection(index))}
          >
            <div>{item.header}</div>
            <div className={css.openIcon}>
              +
            </div>
          </div>
          {/* {openSection === index && ( */}
          <CSSTransition
            in={openSection === index}
            classNames="item-display"
            unmountOnExit
            timeout={300}
          >
            <div
              className={css.content}
              style={{
                outflow: 'hidden',
              }}
            >
              {item.content}
            </div>
          </CSSTransition>
          {/* )} */}
        </div>
      ))}
    </div>
  );
};

Accordion.propTypes = {
  list: arrayOf(shape({
    header: string,
    content: string,
  })).isRequired,
};

export default Accordion;
