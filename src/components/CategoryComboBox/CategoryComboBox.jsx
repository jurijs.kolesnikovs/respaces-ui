import React, { useState } from 'react';
import { string, func, arrayOf, shape } from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import { intlShape } from '../../util/reactIntl';

import { ComboBox } from '..';

import css from './CategoryComboBox.module.scss';

const Item = ({ onClick, id, labelId }) => (
  <li className={css.option} key={`category_combobox_${id}`}>
    <button
      type="button"
      className={css.optionButton}
      onClick={() => onClick(id)}
    >
      <FormattedMessage id={labelId}>
        {intlId => <p className={css.optionText}>{intlId}</p>}
      </FormattedMessage>
    </button>
  </li>
);

Item.propTypes = {
  labelId: string.isRequired,
  id: string.isRequired,
  onClick: func.isRequired,
};

const CategoryComboBoxComponent = ({
  id,
  name,
  onChange,
  currentCategory,
  intl,
  categories,
}) => {
  const formattedCategories = categories.map(c => ({
    id: c.key,
    label: c.label,
    labelId: c.labelId,
  }));

  const placeholder = intl.formatMessage({
    id: 'SectionHero.typePlaceholder',
  });

  const [ isOpen, toggle ] = useState(false);

  const onClick = val => {
    toggle(false);
    onChange(val);
  };

  const inputValue = formattedCategories.find(c => c.id === currentCategory)?.labelId || null;

  let inputFormattedValue;

  if (inputValue) {
    inputFormattedValue = intl.formatMessage({
      id: inputValue,
    });
  }

  return (
    <ComboBox
      id={id}
      name={name}
      isOpen={isOpen}
      value={inputFormattedValue}
      onFocus={() => toggle(true)}
      onClose={() => toggle(false)}
      placeholder={placeholder}
      readOnly
    >
      {formattedCategories.map(category => <Item key={`category_${category.key}`} {...category} onClick={onClick} />)}
    </ComboBox>
  );
};

CategoryComboBoxComponent.propTypes = {
  id: string.isRequired,
  name: string.isRequired,
  onChange: func.isRequired,
  currentCategory: string.isRequired,
  intl: intlShape.isRequired,
  categories: arrayOf(shape({
    id: string,
    label: string,
    labelId: string,
  })).isRequired,
};

const CategoryComboBox = injectIntl(CategoryComboBoxComponent);

export default CategoryComboBox;
