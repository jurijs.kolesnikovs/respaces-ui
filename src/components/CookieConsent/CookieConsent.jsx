import React, { useState, useEffect } from 'react';
import { FormattedMessage } from '../../util/reactIntl';

import { Button } from '..';

import css from './CookieConsent.module.scss';

const CookieConsent = () => {
  const [ show, toggleShow ] = useState(false);

  const isServer = typeof document === 'undefined';
  const showCookieConsent = !isServer && !!show;

  useEffect(() => {
    if (typeof document !== 'undefined') {
      const cookies = document.cookie.split('; ').reduce((acc, curr) => {
        const [ name, value ] = curr.split('=');
        return {
          ...acc, [name]: decodeURIComponent(value),
        };
      }, {
      });

      if (cookies.euCookiesAccepted !== '1') {
        toggleShow(true);
      }
    }
  }, []);

  const saveCookieConsent = () => {
    // We create date object and modify it to show date 10 years into the future.
    const expirationDate = new Date();
    expirationDate.setFullYear(expirationDate.getFullYear() + 10);
    // Save the cookie with expiration date
    document.cookie = `euCookiesAccepted=1; path=/; expires=${expirationDate.toGMTString()}`;
  };

  const onAcceptCookies = () => {
    saveCookieConsent();
    toggleShow(false);
  };

  const cookieLink = (
    <a href="https://cookiesandyou.com" className={css.cookieLink}>
      <FormattedMessage id="CookieConsent.cookieLink" />
    </a>
  );
  return showCookieConsent && (
    <div className={css.root}>
      <div className={css.message}>
        <FormattedMessage
          id="CookieConsent.message"
          values={{
            cookieLink,
          }}
        />
      </div>
      {/* TODO: Replace with custom Button component */}
      <Button type="outlined" borderType="rounded" size="xs" className={css.continueBtn} onClick={onAcceptCookies}>
        <FormattedMessage id="CookieConsent.continue" />
      </Button>
    </div>
  );
};

export default CookieConsent;
