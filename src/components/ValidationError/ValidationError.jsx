import React from 'react';
import classNames from 'classnames';
import { shape, bool, string } from 'prop-types';

import css from './ValidationError.module.scss';

const ValidationError = props => {
  const { rootClassName, className, fieldMeta } = props;
  const { touched, error } = fieldMeta;
  const classList = classNames(rootClassName, css.root, className);

  return touched && error ? <div className={classList}>{error}</div> : null;
};

ValidationError.defaultProps = {
  rootClassName: null,
  className: null,
};

ValidationError.propTypes = {
  rootClassName: string,
  className: string,
  fieldMeta: shape({
    touched: bool.isRequired,
    error: string,
  }).isRequired,
};

export default ValidationError;
