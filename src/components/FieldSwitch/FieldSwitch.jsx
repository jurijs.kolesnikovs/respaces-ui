import React from 'react';
import classNames from 'classnames';
import { Field } from 'react-final-form';
import { string, bool, oneOf } from 'prop-types';

import css from './FieldSwitch.module.scss';

export const COLORS = {
  WHITE: 'white',
  GREEN: 'green',
};

COLORS.LIST = [
  COLORS.WHITE,
  COLORS.GREEN,
];

const InputToggle = ({
  id,
  name,
  disabled,
  color,
  isChecked,
  withIcon,
  hideIconOnOff,
}) => {
  const toggleClassList = classNames(
    css.toggle,
    css[color],
    isChecked && css.active,
    disabled && css.disabled,
  );

  const circleClassList = classNames(css.circle, isChecked && css.active);
  const iconClassList = classNames(
    css.icon,
    isChecked && css.active,
    !isChecked && hideIconOnOff && css.hidden,
  );

  const fieldProps = {
    id,
    name,
    component: 'input',
    type: 'checkbox',
    className: css.input,
  };

  return (
    <label htmlFor={id}>
      <Field {...fieldProps} />
      <div className={toggleClassList}>
        <div className={circleClassList}>
          {withIcon && (
            <svg
              className={iconClassList}
              width="13"
              height="11"
              viewBox="0 0 13 11"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M11.6666 1.73364L4.33332 9.40152L1 5.91612"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          )}
        </div>
      </div>
    </label>
  );
};

InputToggle.defaultProps = {
  disabled: false,
  color: COLORS.GREEN,
  withIcon: true,
  hideIconOnOff: false,
};

InputToggle.propTypes = {
  id: string.isRequired,
  name: string.isRequired,
  disabled: bool,
  color: oneOf(COLORS.LIST),
  isChecked: bool.isRequired,
  withIcon: bool,
  hideIconOnOff: bool,
};

export default InputToggle;
