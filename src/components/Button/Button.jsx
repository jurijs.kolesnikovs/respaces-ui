import React from 'react';
import classNames from 'classnames';
import {
  Link,
  withRouter,
} from 'react-router-dom';
import {
  string,
  bool,
  object,
  any,
  func,
  shape,
  oneOf,
} from 'prop-types';

import IconCheckmark from './components/IconCheckmark';
import IconSpinner from './components/IconSpinner';

import css from './Button.module.scss';

export const BUTTON_TYPES = {
  PRIMARY: 'primary',
  SECONDARY: 'secondary',
  OUTLINED: 'outlined',
  RED: 'red',
  GREEN: 'green',
  WHITE: 'white',
  INLINE: 'inline',
};

BUTTON_TYPES.LIST = [
  BUTTON_TYPES.PRIMARY,
  BUTTON_TYPES.SECONDARY,
  BUTTON_TYPES.OUTLINED,
  BUTTON_TYPES.RED,
  BUTTON_TYPES.GREEN,
  BUTTON_TYPES.WHITE,
  BUTTON_TYPES.INLINE,
];

export const BUTTON_SIZES = {
  X_SMALL: 'xs',
  SMALL: 's',
  MEDIUM: 'm',
  LARGE: 'l',
};

BUTTON_SIZES.LIST = [ BUTTON_SIZES.SMALL, BUTTON_SIZES.MEDIUM, BUTTON_SIZES.LARGE ];

export const LINK_COLORS = {
  BLACK: 'black',
  BLUE: 'blue',
  WHITE: 'white',
};

LINK_COLORS.LIST = [ LINK_COLORS.BLACK, LINK_COLORS.BLUE, LINK_COLORS.WHITE ];

export const BORDER_RADIUS_TYPES = {
  NONE: 'sharp',
  ROUNDED: 'rounded',
  OVAL: 'oval',
};

BORDER_RADIUS_TYPES.LIST = [
  BORDER_RADIUS_TYPES.NONE,
  BORDER_RADIUS_TYPES.ROUNDED,
  BORDER_RADIUS_TYPES.OVAL,
];

const SPINNER_COLOR = {
  primary: 'white',
  secondary: 'black',
  red: 'white',
  green: 'white',
  white: 'black',
};

const Arrow = ({ className }) => (
  <svg
    className={className}
    width="18"
    height="14"
    viewBox="0 0 18 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M0 7H16M16 7L10.1818 1M16 7L10.1818 13" strokeWidth="2" />
  </svg>
);

Arrow.propTypes = {
  className: string.isRequired,
};

const ButtonComponent = props => {
  const {
    style,
    className,
    type,
    size,
    borderType,
    linkColor,
    textLight,
    withIcon,
    block,
    underlined,
    name,
    params,
    to,
    children,
    match,
    inProgress,
    ready,
    disabled,
    onClick,
  } = props;

  if (!children) {
    throw new Error('No children provided!');
  }

  const buttonClassList = classNames(
    css.button,
    className,
    css[type],
    css[size],
    css[borderType],
    block && css.block,
    textLight && css.textLight,
  );

  let content;

  if (inProgress) {
    content = <IconSpinner className={SPINNER_COLOR[type]} />;
  } else if (ready) {
    content = <IconCheckmark className={SPINNER_COLOR[type]} />;
  } else {
    content = children;
  }

  // All buttons are disabled until the component is mounted. This
  // prevents e.g. being able to submit forms to the backend before
  // the client side is handling the submit.

  const linkClassList = classNames(buttonClassList, css.link, css[linkColor], {
    /* [css.active]: match.url && match.url === name, */
    [css.linkUnderlined]: underlined,
    [css.disabled]: disabled,
  });

  const arrowClassList = classNames(css.arrow, css[linkColor]);

  /* Name is sufficient prop to render a link to other route.
    * If there is no name provided and there is a text (children),
    * button is rendered */
  return name ? (
    <Link
      style={style}
      className={linkClassList}
      to={{
        pathname: name, ...to,
      }}
      match={match}
      params={params}
    >
      {content}
      {withIcon && type === 'inline' && <Arrow className={arrowClassList} /> }
    </Link>
  ) : content && (
    <button style={style} type="button" onClick={onClick} className={buttonClassList} disabled={disabled}>
      {content}
      {withIcon && type === 'inline' && <Arrow className={arrowClassList} /> }
    </button>
  );
};

ButtonComponent.defaultProps = {
  style: null,
  className: null,
  block: false,
  underlined: false,
  type: BUTTON_TYPES.PRIMARY,
  size: BUTTON_SIZES.SMALL,
  linkColor: LINK_COLORS.BLACK,
  borderType: BORDER_RADIUS_TYPES.OVAL,
  textLight: false,
  withIcon: false,
  name: null,
  params: null,
  to: null,
  match: null,
  inProgress: false,
  ready: false,
  disabled: false,
  onClick: null,
};

ButtonComponent.propTypes = {
  /* Styles */
  // ESLint disabled because params keys can be really different
  // eslint-disable-next-line react/forbid-prop-types
  style: object,
  className: string,
  block: bool,
  underlined: bool,
  type: oneOf(BUTTON_TYPES.LIST),
  size: oneOf(BUTTON_SIZES.LIST),
  linkColor: oneOf(LINK_COLORS.LIST),
  borderType: oneOf(BORDER_RADIUS_TYPES.LIST),
  textLight: bool,
  withIcon: bool,
  // eslint-disable-next-line react/forbid-prop-types
  children: any.isRequired,
  /* Navigation props */
  name: string,
  // ESLint disabled because params keys can be really different
  // eslint-disable-next-line react/forbid-prop-types
  params: object, // params object for the named route.
  to: shape({
    search: string,
    hash: string,
    state: shape({
    }),
  }), // Link component props
  match: shape({
    url: string,
  }), // from withRouter
  /* State */
  inProgress: bool,
  ready: bool,
  disabled: bool,
  onClick: func,
};

const Button = withRouter(ButtonComponent);

export default Button;
