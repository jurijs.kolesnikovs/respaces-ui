import React from 'react';
import { number, shape, string } from 'prop-types';

import { RatingStars } from '..';
import { STAR_SIZES } from '../RatingStars/RatingStars';
import { FILL_COLORS } from '../IconReviewStar/IconReviewStar';
import { FormattedMessage } from '../../util/reactIntl';

import css from './ReviewCard.module.scss';

const ReviewCard = ({ review: { rating, text, name } }) => (
  <div className={css.reviewCard}>
    <RatingStars
      rating={rating}
      condensed={false}
      size={STAR_SIZES.SMALL}
      fillColor={FILL_COLORS.GOLD}
    />
    <FormattedMessage id={text}>{id => <p className={css.text}>{id}</p>}</FormattedMessage>
    <h5>{name}</h5>
  </div>
);

ReviewCard.propTypes = {
  review: shape({
    rating: number,
    text: string,
    name: string,
  }).isRequired,
};

export default ReviewCard;
