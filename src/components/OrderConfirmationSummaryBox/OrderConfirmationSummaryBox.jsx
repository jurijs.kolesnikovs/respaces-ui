import React from 'react';
import moment from 'moment';
import classNames from 'classnames';
import { injectIntl } from 'react-intl';

import { Button } from '../index';

import css from './OrderConfirmationSummaryBox.module.scss';

const OrderConfirmationSummaryBox = () => {
  const transaction = {
    id: {
      _sdkType: 'UUID',
      uuid: '60cb0cba-092c-42ec-917f-cdadc210d81b',
    },
    type: 'transaction',
    attributes: {
      processName: 'flex-default-process',
      transitions: [
        {
          transition: 'transition/request-payment-daily',
          createdAt: '2021-06-17T08:50:03.194Z',
          by: 'customer',
        },
        {
          transition: 'transition/confirm-payment',
          createdAt: '2021-06-17T08:50:08.937Z',
          by: 'customer',
        },
        {
          transition: 'transition/disable-cancellation',
          createdAt: '2021-06-20T00:01:22.916Z',
          by: 'system',
        },
        {
          transition: 'transition/complete',
          createdAt: '2021-06-24T00:01:46.762Z',
          by: 'system',
        },
      ],
      payoutTotal: {
        _sdkType: 'Money',
        amount: 3600,
        currency: 'SEK',
      },
      processVersion: 15,
      createdAt: '2021-06-17T08:50:02.367Z',
      lastTransitionedAt: '2021-06-24T00:01:46.762Z',
      protectedData: {
        bookingType: 'daily',
        lineItems: [ {
          seats: 1,
          lineTotal: {
            amount: 45, currency: 'SEK',
          },
          code: 'line-item/units',
          includeFor: [ 'customer', 'provider' ],
          units: 3,
          unitPrice: {
            amount: 15, currency: 'SEK',
          },
          vat: {
            amount: '2.55', currency: 'SEK',
          },
        }, {
          lineTotal: {
            amount: -9, currency: 'SEK',
          },
          quantity: 1,
          code: 'line-item/provider-commission',
          includeFor: [ 'provider' ],
          unitPrice: {
            amount: -9, currency: 'SEK',
          },
        } ],
        totalSavings: {
          amount: null, currency: null,
        },
        totalVAT: {
          amount: 2.55, currency: 'SEK',
        },
        type: 'days',
      },
      lineItems: [ {
        code: 'line-item/units',
        unitPrice: {
          _sdkType: 'Money', amount: 1500, currency: 'SEK',
        },
        lineTotal: {
          _sdkType: 'Money', amount: 4500, currency: 'SEK',
        },
        reversal: false,
        includeFor: [ 'customer', 'provider' ],
        quantity: '3',
        seats: 1,
        units: '3',
      }, {
        code: 'line-item/provider-commission',
        unitPrice: {
          _sdkType: 'Money', amount: -900, currency: 'SEK',
        },
        lineTotal: {
          _sdkType: 'Money', amount: -900, currency: 'SEK',
        },
        reversal: false,
        includeFor: [ 'provider' ],
        quantity: '1',
      } ],
      lastTransition: 'transition/complete',
      payinTotal: {
        _sdkType: 'Money', amount: 4500, currency: 'SEK',
      },
      metadata: {
      },
    },
    booking: {
      id: {
        _sdkType: 'UUID', uuid: '60cb0cba-721f-47ba-b8bd-3d9e1865ffa2',
      },
      type: 'booking',
      attributes: {
        seats: 4,
        start: new Date('2021-06-21T00:00:00.000Z'),
        end: new Date('2021-06-24T00:00:00.000Z'),
        displayStart: new Date('2021-06-21T00:00:00.000Z'),
        displayEnd: new Date('2021-06-24T00:00:00.000Z'),
        state: 'accepted',
      },
    },
    listing: {
      id: {
        _sdkType: 'UUID', uuid: '5ffbaed3-e292-43a0-9693-0d023113123b',
      },
      type: 'listing',
      attributes: {
        title: 'Private space',
        description: 'Sed porttitor lectus nibh. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Proin eget tortor risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.',
        publicData: {
          location: {
            address: 'Stockholm, Sweden', building: '',
          },
          additionalServices: [ {
            name: 'Lunch',
            price: {
              amount: 500, currency: 'SEK',
            },
            vatPercentage: '6',
            multiplySeats: true,
            serviceId: 'a04a25f7-7d69-4bf0-98d1-bff2f5975679',
          } ],
          ameneties: [ 'pets' ],
          amenetiesInfo: [],
          bookingEmailAddress: [ '', '' ],
          bookingType: 'daily',
          category: 'private',
          contactInfo: {
            contactEmail: '', phoneNumber: '', website: '',
          },
          emailConfirmationContent: '',
          keywords: [ 'central' ],
          maxSeats: 4,
          numberOfInstance: 2,
          offer: [ 'wifi' ],
          paddingTime: {
            amount: 3, unit: 'day',
          },
          perfectFor: [ 'officeFocus' ],
          vatPercentage: '6',
        },
        deleted: false,
        geolocation: {
          _sdkType: 'LatLng', lat: 59.33333, lng: 18.16667,
        },
        createdAt: '2021-01-11T01:50:11.071Z',
        state: 'published',
        availabilityPlan: {
          type: 'availability-plan/day',
        },
        price: {
          _sdkType: 'Money', amount: 1500, currency: 'SEK',
        },
        metadata: {
        },
      },
      author: {
        id: {
          _sdkType: 'UUID', uuid: '5f3ba825-e013-4b53-b11e-83cef5638491',
        },
        type: 'user',
        attributes: {
          profile: {
            abbreviatedName: 'NT',
            displayName: 'NGUYEN T',
            bio: null,
            publicData: {
            },
          },
          banned: false,
          deleted: false,
          createdAt: '2020-08-18T10:06:30.082Z',
        },
        profileImage: null,
      },
      images: [ {
        id: {
          _sdkType: 'UUID', uuid: '5ffbaf19-f102-4f2c-b8fd-3d0719484251',
        },
        type: 'image',
        attributes: {
          variants: {
            'square-small2x': {
              height: 480, width: 480, url: 'https://sharetribe.imgix.net/5ee9d076-2e46-43d1-928c-c0228cb28c91%2F5ffbaf19-f102-4f2c-b8fd-3d0719484251?auto=format&crop=edges&fit=crop&h=480&ixlib=java-1.1.1&w=480&s=8d09c167e8242d8507f755245c3dcaf2', name: 'square-small2x',
            },
            'square-small': {
              height: 240, width: 240, url: 'https://sharetribe.imgix.net/5ee9d076-2e46-43d1-928c-c0228cb28c91%2F5ffbaf19-f102-4f2c-b8fd-3d0719484251?auto=format&crop=edges&fit=crop&h=240&ixlib=java-1.1.1&w=240&s=5f52f714c17707a6dbe4726d03e83c5f', name: 'square-small',
            },
            'landscape-crop': {
              height: 267, width: 400, url: 'https://sharetribe.imgix.net/5ee9d076-2e46-43d1-928c-c0228cb28c91%2F5ffbaf19-f102-4f2c-b8fd-3d0719484251?auto=format&crop=edges&fit=crop&h=267&ixlib=java-1.1.1&w=400&s=5ca580e8942b8bc01b0628967653b514', name: 'landscape-crop',
            },
            'landscape-crop2x': {
              height: 533, width: 800, url: 'https://sharetribe.imgix.net/5ee9d076-2e46-43d1-928c-c0228cb28c91%2F5ffbaf19-f102-4f2c-b8fd-3d0719484251?auto=format&crop=edges&fit=crop&h=533&ixlib=java-1.1.1&w=800&s=89f4012875b6cee3fe9efe47dd0b93ce', name: 'landscape-crop2x',
            },
          },
        },
      } ],
    },
    provider: {
      id: {
        _sdkType: 'UUID', uuid: '5f3ba825-e013-4b53-b11e-83cef5638491',
      },
      type: 'user',
      attributes: {
        profile: {
          abbreviatedName: 'NT',
          displayName: 'NGUYEN T',
          bio: null,
          publicData: {
          },
        },
        banned: false,
        deleted: false,
        createdAt: '2020-08-18T10:06:30.082Z',
      },
      profileImage: null,
    },
    customer: {
      attributes: {
        profile: {
          abbreviatedName: 'OG',
          displayName: 'Oskar G',
          bio: null,
          publicData: {
          },
        },
        banned: false,
        deleted: false,
        createdAt: '2020-06-29T15:37:14.407Z',
      },
      id: {
        _sdkType: 'UUID', uuid: '5efa0aaa-da19-475b-90a4-a9c1b62d21c8',
      },
      type: 'user',
      profileImage: null,
    },
    reviews: [],
  };

  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  const days = [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ];

  const isMeetingSpace = transaction?.listing?.attributes?.publicData?.category === 'meeting';

  const { start, end } = transaction.booking.attributes;

  const startYear = start.getFullYear();
  const startMonth = months[start.getMonth()];
  const endMonth = months[end.getMonth()];
  const startDay = days[start.getDay()];
  const endDay = days[end.getDay()];
  const startDate = start.getDate();
  const endDate = end.getDate();

  const endDateCalculated = moment(end).subtract(1, 'days');

  const getStartDate = () => `${startMonth} ${startDate}, ${startYear}.`;

  const getEndDate = () => (isMeetingSpace
    ? `${endMonth} ${endDate}, ${end.getFullYear()}.`
    : `${
      months[endDateCalculated.month()]
    } ${endDateCalculated.date()}, ${endDateCalculated.year()}.`);

  const getStartTime = () => (isMeetingSpace ? `${startDay} ${start.toLocaleTimeString().slice(0, -3)}` : `${startDay}`);

  const getEndTime = () => (isMeetingSpace
    ? `${endDay} ${end.toLocaleTimeString().slice(0, -3)}`
    : `${days[endDateCalculated.weekday()]}`);

  const getStartEndTimeMaybe = () => (isMeetingSpace
    ? `${start.toLocaleTimeString().slice(0, -3)} - ${end.toLocaleTimeString().slice(0, -3)}`
    : null);

  return (
    <div style={{
      maxWidth: '900px', margin: 'auto',
    }}
    >
      <div className={css.summaryBox}>
        <div className={css.leftSide}>
          <div className={css.viewBookingsMobile}>
            <Button type="inline" size="s" name="/inbox/orders" underlined textLight>View my bookings</Button>
          </div>
          <div className={css.bookingDate}>
            <h1 className={css.bookingDateDay}>21</h1>
            <p className={css.bookingDateMonth}>June</p>
          </div>
        </div>
        <div className={css.rightSide}>
          <div className={css.rightSideTop}>
            <div className={classNames(css.item, css.first)}>
              <h5 className={css.title}>Office Space</h5>
              <h5 className={css.title}>SEK 15.00</h5>
            </div>
            <div className={css.item}>
              <div>
                <p className={css.text}>Private space</p>
                <p className={css.text}>Stockholm, Sweden</p>
              </div>
              <div className={css.viewBookingsDesktop}>
                <Button name="/inbox/orders">View my bookings</Button>
              </div>
            </div>
          </div>
          <div className={css.rightSideBottom}>
            <div className={classNames(css.item, css.bookingInfo)}>
              <div className={css.bookingStart}>
                <h5 className={css.title}>Booking Start</h5>
                <div className={css.bookingText}>
                  {getStartDate()}
                  {getStartTime()}
                  <p className={css.text}>Mon</p>
                  <p className={css.text}>June 21, 2021.</p>
                </div>
              </div>
              <div className={css.bookingEnd}>
                <h5 className={css.title}>Booking End</h5>
                <div className={css.bookingText}>
                  {getEndDate()}
                  {getEndTime()}
                  {getStartEndTimeMaybe()}
                  <p className={css.text}>Wed</p>
                  <p className={css.text}>June 23, 2021.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default injectIntl(OrderConfirmationSummaryBox);
