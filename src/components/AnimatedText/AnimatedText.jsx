import React from 'react';
import { ImageSlider } from '../index';

import { injectIntl, intlShape } from '../../util/reactIntl';

import css from './AnimatedText.module.scss';

const animatedTextOptions = {
  category: 'text',
  autoplay: {
    delay: 3000,
  },
  direction: 'vertical',
  allowTouchMove: false,
};

const AnimatedText = ({ intl }) => {
  const spaceTypes = [
    intl.formatMessage({
      id: 'SectionHero.category.coWorking',
    }),
    intl.formatMessage({
      id: 'SectionHero.category.private',
    }),
    intl.formatMessage({
      id: 'SectionHero.category.meeting',
    }),
    intl.formatMessage({
      id: 'SectionHero.category.studio',
    }),
  ];

  return (
    <div className={css.animatedTextWrapper}>
      <ImageSlider
        className={css.animatedText}
        data={spaceTypes}
        {...animatedTextOptions}
      />
    </div>
  );
};

AnimatedText.propTypes = {
  intl: intlShape.isRequired,
};

export default injectIntl(AnimatedText);
