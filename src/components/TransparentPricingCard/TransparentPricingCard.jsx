import React from 'react';
import classNames from 'classnames';
import { shape, bool, number, string } from 'prop-types';

import { FormattedMessage } from '../../util/reactIntl';

import css from './TransparentPricingCard.module.scss';

const TransparentPricingCard = ({ item: { days, price }, isFavorite }) => {
  const pricingCardClassList = classNames(css.pricingCard, {
    [css.popular]: isFavorite,
  });

  return (
    <div className={pricingCardClassList}>
      <FormattedMessage
        id="SectionTransparentPricing.packagesOfferTitle"
        values={{
          num: days,
        }}
      >
        {id => <h5 className={css.pricingCardTitle}>{id}</h5>}
      </FormattedMessage>
      <p className={css.pricingCardPrice}>{price}</p>
      <FormattedMessage id="SectionTransparentPricing.packagesOfferPrice">
        {id => <p className={css.pricingCardRatio}>{id}</p>}
      </FormattedMessage>
      {
        isFavorite && (
          <div className={css.pricingCardRibbon}>
            <FormattedMessage id="SectionTransparentPricing.ribbon">
              {id => <span>{id}</span>}
            </FormattedMessage>
          </div>
        )
      }
    </div>
  );
};

TransparentPricingCard.defaultProps = {
  isFavorite: false,
};

TransparentPricingCard.propTypes = {
  item: shape({
    days: number, price: string,
  }).isRequired,
  isFavorite: bool,
};

export default TransparentPricingCard;
