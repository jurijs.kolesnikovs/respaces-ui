import React from 'react';
import classNames from 'classnames';
import { any, oneOf } from 'prop-types';

import css from './Title.module.scss';

export const PRIORITY = {
  h1: 'h1',
  h2: 'h2',
  h3: 'h3',
  h4: 'h4',
  h5: 'h5',
};

PRIORITY.LIST = [
  PRIORITY.h1,
  PRIORITY.h2,
  PRIORITY.h3,
  PRIORITY.h4,
  PRIORITY.h5,
];

const Title = ({ priority, children }) => {
  const TitleTag = `${PRIORITY[priority]}`;

  const titleClassList = classNames(css.title, css[`title_${priority}`]);

  return <TitleTag className={titleClassList}>{children}</TitleTag>;
};

Title.defaultProps = {
  priority: PRIORITY.h1,
  children: null,
};

Title.propTypes = {
  priority: oneOf(PRIORITY.LIST),
  // eslint-disable-next-line react/forbid-prop-types
  children: any,
};

export default Title;
