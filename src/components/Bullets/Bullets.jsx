import React from 'react';
import classNames from 'classnames';
import {
  bool,
  oneOf,
  string,
  arrayOf,
} from 'prop-types';

import css from './Bullets.module.scss';

export const BULLET_TYPES = [ 'regular', 'check' ];
export const CHECKMARK_COLORS = [ 'black', 'green' ];

export const GRID_COUNT = {
  THREE: 3,
  FOUR: 4,
};

GRID_COUNT.LIST = [
  GRID_COUNT.THREE,
  GRID_COUNT.FOUR,
];

const CheckMark = ({ checkmarkClassList }) => (
  <svg
    className={checkmarkClassList}
    width="15"
    height="11"
    viewBox="0 0 15 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M1.5 5L5.5 9L13.5 1" fill="#fff" strokeWidth="2" />
  </svg>
);

CheckMark.propTypes = {
  checkmarkClassList: string.isRequired,
};

const Bullets = props => {
  const { bulletType, bullets, checkColor, displayAsGrid, gridCount } = props;

  const bulletListClassList = classNames(
    css.bullets,
    bulletType === 'check' && css.bulletsWithCheckmark,
    displayAsGrid && css.grid,
    displayAsGrid && css[`grid${gridCount}`],
  );
  const bulletClassList = classNames(css.bulletsItem, css[bulletType]);
  const checkmarkClassList = classNames(css.checkmark, css[checkColor]);

  return (
    <ul className={bulletListClassList}>
      {bullets && bullets.length && bullets.map(bullet => (
        <li className={bulletClassList}>
          {bulletType === 'check' && (
            <CheckMark checkmarkClassList={checkmarkClassList} />
          )}
          <span>{bullet}</span>
        </li>
      ))}
    </ul>
  );
};

Bullets.defaultProps = {
  bulletType: BULLET_TYPES[0],
  checkColor: CHECKMARK_COLORS[0],
  displayAsGrid: false,
  gridCount: null,
};

Bullets.propTypes = {
  bulletType: oneOf(BULLET_TYPES),
  checkColor: oneOf(CHECKMARK_COLORS),
  bullets: arrayOf(string).isRequired,
  displayAsGrid: bool,
  gridCount: oneOf(GRID_COUNT.LIST),
};

export default Bullets;
