import React from 'react';
import classNames from 'classnames';
import { any, bool, oneOf } from 'prop-types';

import css from './Text.module.scss';

export const SIZES = {
  SMALL: 's',
  MEDIUM: 'm',
  LARGE: 'l',
};

SIZES.LIST = [
  SIZES.SMALL,
  SIZES.MEDIUM,
  SIZES.LARGE,
];

export const WEIGHTS = {
  LIGHT: 'light',
  REGULAR: 'regular',
  BOLD: 'bold',
};

WEIGHTS.LIST = [
  WEIGHTS.LIGHT,
  WEIGHTS.REGULAR,
  WEIGHTS.BOLD,
];

const Text = ({ size, weight, isUpperCased, isUnderlined, children }) => {
  const textClassList = classNames(
    css.text,
    css[size],
    weight && css[weight],
    isUpperCased && css.uppercase,
    isUnderlined && css.underline,
  );

  return (
    <p className={textClassList}>{children}</p>
  );
};

Text.defaultProps = {
  size: SIZES.MEDIUM,
  weight: null,
  isUpperCased: false,
  isUnderlined: false,
};

Text.propTypes = {
  size: oneOf(SIZES.LIST),
  weight: oneOf(WEIGHTS.LIST),
  isUpperCased: bool,
  isUnderlined: bool,
  // eslint-disable-next-line react/forbid-prop-types
  children: any.isRequired,
};

export default Text;
