import React, { useRef } from 'react';
import classNames from 'classnames';
import {
  string,
  func,
  oneOf,
  bool,
  any,
} from 'prop-types';

import SearchIcon from './SearchIcon';

import css from './ComboBox.module.scss';

const BORDER_TYPES = [ 'center', 'left', 'right', 'rounded', 'none' ];

const ComboBox = props => {
  const {
    id,
    name,
    value,
    isOpen,
    borderType,
    placeholder,
    readOnly,
    children,
    onInput,
    onFocus,
    onClose,
  } = props;

  const comboBoxClassList = classNames(css.combobox, {
    [css.focused]: isOpen,
  }, css[borderType]);
  const dropdownClassList = classNames(css.dropdown, isOpen && css.open);
  const overlayClassList = classNames(css.overlay, isOpen && css.open);
  const iconClasses = classNames(css.searchIcon, {
    [css.searchIconFocused]: isOpen,
  });

  const comboBox = useRef(null);
  const dropdown = useRef(null);

  const openComboBox = () => {
    onFocus();

    const comboBoxDimensions = comboBox.current.getBoundingClientRect();
    /* const dropdownDimensions = dropdown.current.getBoundingClientRect(); */

    /* const { height } = dropdownDimensions; */
    const topSpace = comboBoxDimensions.top;
    const windowHeight = document.documentElement.clientHeight;
    const bottomSpaceForWindowTop = comboBoxDimensions.bottom;
    const bottomSpace = windowHeight - bottomSpaceForWindowTop;

    /* const { width } = dropdownDimensions;
    const leftSpace = comboBoxDimensions.left;
    const windowWidth = document.documentElement.clientWidth;
    const rightSpaceForWindowLeft = comboBoxDimensions.right; */
    /* const rightSpace = windowWidth - rightSpaceForWindowLeft; */

    /* if (height > bottomSpace && topSpace > bottomSpace) { setShowTop(true); }
    if (width > rightSpace && leftSpace > rightSpace) { setShowLeft(true); } */

    dropdown.current.style.maxHeight = `${Math.max(topSpace, bottomSpace) - 20}px`;

    if (dropdown.current.scrollHeight > dropdown.current.clientHeight) {
      dropdown.current.style.overflowY = 'scroll';
    }
  };

  return (
    <>
      <div className={comboBoxClassList} ref={comboBox}>
        <div className={css.iconWrapper}>
          <SearchIcon className={iconClasses} />
        </div>
        <input
          name={name}
          id={id}
          className={css.input}
          value={value}
          onFocus={openComboBox}
          onInput={e => onInput(e.target.value)}
          type="text"
          placeholder={placeholder}
          autoComplete="off"
          readOnly={readOnly}
        />
        <div ref={dropdown} className={dropdownClassList}>{children}</div>
      </div>
      <div className={overlayClassList} onClick={onClose} onKeyDown={() => null} aria-hidden="true" />
    </>
  );
};

ComboBox.defaultProps = {
  id: null,
  value: null,
  readOnly: false,
  borderType: BORDER_TYPES[0],
  onClose: null,
  onFocus: null,
  onInput: null,
};

ComboBox.propTypes = {
  id: string,
  readOnly: bool,
  value: string,
  placeholder: string.isRequired,
  borderType: oneOf(BORDER_TYPES),
  name: string.isRequired,
  isOpen: bool.isRequired,
  onClose: func,
  onFocus: func,
  onInput: func,
  // eslint-disable-next-line react/forbid-prop-types
  children: any.isRequired,
};

export default ComboBox;
