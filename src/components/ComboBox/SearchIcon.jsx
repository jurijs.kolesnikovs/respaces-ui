import React from 'react';
import { string } from 'prop-types';

const SearchIcon = ({ className }) => (
  <svg
    width="15"
    height="15"
    viewBox="0 0 15 15"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    className={className}
  >
    <g clipPath="url(#clip0)">
      <path d="M5.43058 10.8319C2.4341 10.8319 0 8.39755 0 5.41156C0 2.42557 2.4341 0 5.43058 0C8.42706 0 10.8612 2.42557 10.8612 5.41156C10.8612 8.39755 8.42706 10.8319 5.43058 10.8319ZM5.43058 1.19965C3.10193 1.19965 1.20387 3.09107 1.20387 5.41156C1.20387 7.73205 3.10193 9.63222 5.43058 9.63222C7.75923 9.63222 9.66608 7.74081 9.66608 5.41156C9.66608 3.08231 7.76801 1.19965 5.43058 1.19965Z" />
      <path d="M14.3935 15C14.2353 15 14.086 14.9387 13.9717 14.8249L9.40231 10.2715C9.16505 10.035 9.16505 9.6585 9.40231 9.42207C9.63957 9.18564 10.0174 9.18564 10.2547 9.42207L14.8241 13.9755C15.0613 14.2119 15.0613 14.5884 14.8241 14.8249C14.7098 14.9387 14.5517 15 14.3935 15Z" />
    </g>
    <defs>
      <clipPath id="clip0">
        <rect width="15" height="15" fill="white" />
      </clipPath>
    </defs>
  </svg>
);

SearchIcon.defaultProps = {
  className: null,
};

SearchIcon.propTypes = {
  className: string,
};

export default SearchIcon;
