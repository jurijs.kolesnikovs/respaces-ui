import React from 'react';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { oneOf, string, bool, shape, arrayOf, any } from 'prop-types';

import { Button } from '..';
import { BUTTON_TYPES, BUTTON_SIZES, LINK_COLORS } from '../Button/Button';
import { isMobile } from '../../util/device';

import css from './MediaAndTextSection.module.scss';

export const TEXT_ALIGN = {
  LEFT: 'left',
  RIGHT: 'right',
};

TEXT_ALIGN.LIST = [
  TEXT_ALIGN.LEFT,
  TEXT_ALIGN.RIGHT,
];

export const TEXT_ALIGN_MOBILE = {
  BOTTOM: 'bottom',
  TOP: 'top',
};

TEXT_ALIGN_MOBILE.LIST = [
  TEXT_ALIGN_MOBILE.BOTTOM,
  TEXT_ALIGN_MOBILE.TOP,
];

export const BG_COLORS = {
  LIGHT: 'light',
  DARK: 'dark',
};

BG_COLORS.LIST = [
  BG_COLORS.LIGHT,
  BG_COLORS.DARK,
];

export const SPACINGS = {
  SMALL: 's',
  MEDIUM: 'm',
};

SPACINGS.LIST = [
  SPACINGS.SMALL,
  SPACINGS.MEDIUM,
];

export const BULLET_TYPES = {
  REGULAR: 'regular',
  CHECK: 'check',
};

BULLET_TYPES.LIST = [
  BULLET_TYPES.REGULAR,
  BULLET_TYPES.CHECK,
];

export const CHECKMARK_COLORS = {
  BLACK: 'black',
  GREEN: 'green',
};

CHECKMARK_COLORS.LIST = [
  CHECKMARK_COLORS.BLACK,
  CHECKMARK_COLORS.GREEN,
];

const CheckMark = ({ checkmarkClassList }) => (
  <svg
    className={checkmarkClassList}
    width="15"
    height="11"
    viewBox="0 0 15 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M1.5 5L5.5 9L13.5 1" fill="#fff" strokeWidth="2" />
  </svg>
);

CheckMark.defaultProps = {
  checkmarkClassList: null,
};

CheckMark.propTypes = {
  checkmarkClassList: string,
};

const shouldRenderOnMobile = payload => (isMobile && payload.showMobile) || !isMobile;

const ImageGrid = ({ media }) => {
  const shadowImgClassList = classNames(css.img, css.imgWithShadow);

  const shadowLinkClassList = classNames(css.ghostLink, {
    [css.visible]: isMobile,
  });
  return (
    <div className={css.imgGrid}>
      {media.content.map(item => (
        <div className={css.imgWrapper}>
          <img className={shadowImgClassList} src={item.img || item} alt="" />
          <Link className={shadowLinkClassList} to={item.to} />
          {item.title && (
            <>
              <div className={css.imgText}>
                <h4 className={css.imgTitle}>{item.title}</h4>
                {item.text && <p className={css.imgSubtitle}>{item.text}</p>}
                {item.to && (
                  <Link className={css.imgCtaLink} to={item.to}>
                    <FormattedMessage id="LandingPage.categories.ctaText">
                      {id => <Button className={css.imgCtaButton}>{id}</Button>}
                    </FormattedMessage>
                  </Link>
                )}
              </div>
              <div className={css.imgTextOverlay} />
            </>
          )}
        </div>
      ))}
    </div>
  );
};

ImageGrid.propTypes = {
  media: arrayOf(shape({
    img: string,
    to: string,
    title: string,
    text: string,
  })).isRequired,
};

const MediaWrapper = ({ media, wideMedia }) => {
  const imgWrapperClassList = classNames(css.imgWrapper, {
    [css.wide]: wideMedia,
  });

  return typeof media.content !== 'undefined' && shouldRenderOnMobile(media) ? (
    <div className={css.media}>
      {/* If there's one image, render it. Otherwise render grid of 4 images */}
      {typeof media.content === 'string' ? (
        <div className={imgWrapperClassList}>
          <img className={css.img} src={media.content} alt="" />
        </div>
      ) : (
        <ImageGrid media={media} />
      )}
    </div>
  ) : null;
};

MediaWrapper.defaultProps = {
  wideMedia: false,
};

MediaWrapper.propTypes = {
  media: shape({
    content: oneOf([ string, arrayOf(shape({
      img: string,
      to: string,
      title: string,
      text: string,
    })) ]),
    showMobile: bool,
  }).isRequired,
  wideMedia: bool,
};

const Features = ({ features }) => (features && features.list && shouldRenderOnMobile(features) ? (
  <ul className={css.featuresList}>
    {features.list.map(item => shouldRenderOnMobile(item) && (
      <li className={css.feature} key={`feature_item_${item.title}`}>
        <h5 className={css.featureTitle}>{item.title}</h5>
        <p className={css.featureText}>{item.text}</p>
      </li>
    ))}
  </ul>
) : null);

Features.defaultProps = {
  features: null,
};

Features.propTypes = {
  features: shape({
    list: arrayOf(shape({
      title: string,
      text: string,
      showMobile: bool,
    })),
    showMobile: bool,
  }),
};

const MediaAndTextSection = props => {
  const {
    title,
    subtitle,
    caption,
    bullets,
    bulletType,
    checkColor,
    features,
    media,
    wideMedia,
    textAlign,
    textAlignMobile,
    bgColor,
    spacing,
    buttonProps,
    children,
  } = props;

  const { toScroll, linkText, ...restButtonProps } = buttonProps;

  const containerClassList = classNames(
    css.container,
    bgColor === 'dark' ? css.bgDark : css.bgLight,
    css[`spacing_${spacing}`],
  );

  const contentClassList = classNames(
    css.content,
    textAlign === 'left' ? css.textLeft : css.textRight,
    textAlignMobile === 'bottom' ? css.textMobileBottom : css.textMobileTop,
    media && !media.showMobile && css.withoutMedia,
  );

  const bulletListClassList = classNames(css.bullets, bulletType === 'check' && css.bulletsWithCheckmark);
  const bulletClassList = classNames(css.bulletsItem, css[bulletType]);
  const checkmarkClassList = classNames(css.checkmark, css[checkColor]);

  const scrollToContainer = () => {
    if (typeof document !== 'undefined') {
      document.getElementById(toScroll).scrollIntoView({
        behavior: 'smooth',
      });
    }
  };

  return (
    <div className={containerClassList}>
      <div className={contentClassList}>
        <div className={css.info}>
          {subtitle && <h2 className={css.subtitle}>{subtitle.content}</h2>}
          {title && title.content && shouldRenderOnMobile(title) && (
            <h1 className={css.title}>{title.content}</h1>
          )}
          {caption && caption.text && shouldRenderOnMobile(caption) && (
            <p className={css.text}>{caption.text}</p>
          )}
          {bullets && bullets.list && shouldRenderOnMobile(bullets) && (
            <>
              {bullets.title && <h5 className={css.bulletsTitle}>{bullets.title}</h5>}
              <ul className={bulletListClassList}>
                {bullets.list.map(item => (
                  <li className={bulletClassList}>
                    {bulletType === 'check' && (
                      <CheckMark checkmarkClassList={checkmarkClassList} />
                    )}
                    {item}
                  </li>
                ))}
              </ul>
            </>
          )}
          <Features features={features} />
          {restButtonProps.to && linkText && (
            <Button {...restButtonProps} name={restButtonProps.to}>
              <span className={css.linkText}>{linkText}</span>
            </Button>
          )}
          {toScroll && (
            <Button {...restButtonProps} onClick={scrollToContainer}>
              <span className={css.linkText}>{linkText}</span>
            </Button>
          )}
        </div>
        {children ? (
          <div className={css.media}>
            <div className={css.childWrapper}>{children}</div>
          </div>
        ) : (
          <MediaWrapper media={media} wideMedia={wideMedia} />
        )}
      </div>
    </div>
  );
};

MediaAndTextSection.defaultProps = {
  title: null,
  subtitle: null,
  caption: null,
  bullets: null,
  bulletType: BULLET_TYPES.REGULAR,
  checkColor: CHECKMARK_COLORS.BLACK,
  features: null,
  media: null,
  wideMedia: false,
  textAlign: TEXT_ALIGN.LEFT,
  textAlignMobile: TEXT_ALIGN_MOBILE.BOTTOM,
  bgColor: BG_COLORS.LIGHT,
  spacing: SPACINGS.SMALL,
  buttonProps: {
    withIcon: false,
    to: null,
    toScroll: null,
    type: BUTTON_TYPES.INLINE,
    size: BUTTON_SIZES.SMALL,
    linkColor: LINK_COLORS.BLUE,
  },
};

MediaAndTextSection.propTypes = {
  title: shape({
    content: string,
    showMobile: bool,
  }),
  subtitle: shape({
    content: string,
    showMobile: bool,
  }),
  caption: shape({
    text: string,
    showMobile: bool,
  }),
  bullets: shape({
    list: arrayOf(string),
    showMobile: bool,
  }),
  bulletType: oneOf(BULLET_TYPES.LIST),
  checkColor: oneOf(CHECKMARK_COLORS.LIST),
  features: shape({
    list: arrayOf(shape({
      title: string,
      text: string,
      showMobile: bool,
    })),
    showMobile: bool,
  }),
  media: shape({
    // eslint-disable-next-line react/forbid-prop-types
    content: any,
    showMobile: bool,
  }),
  wideMedia: bool,
  textAlign: oneOf(TEXT_ALIGN.LIST),
  textAlignMobile: oneOf(TEXT_ALIGN_MOBILE.LIST),
  bgColor: oneOf(BG_COLORS.LIST),
  spacing: oneOf(SPACINGS.LIST),
  buttonProps: shape({
    withIcon: bool,
    linkText: string.isRequired,
    to: string,
    toScroll: string,
    type: oneOf(BUTTON_TYPES.LIST),
    size: oneOf(BUTTON_SIZES.LIST),
    linkColor: oneOf(LINK_COLORS.LIST),
  }),
};

export default MediaAndTextSection;
