import React from 'react';
import { arrayOf, string } from 'prop-types';

import { FieldSelect } from '..';
import { injectIntl, intlShape } from '../../util/reactIntl';

const LanguageSelect = ({ languages, intl, optionToBlock, ...restProps }) => {
  const languageOptions = [ '', ...languages ];

  return (
    <FieldSelect
      {...restProps}
      meta={{
        valid: true, touched: false,
      }}
    >
      {
        languageOptions.map(lang => (
          <option key={lang} value={lang} disabled={lang === optionToBlock}>
            {intl.formatMessage({
              id: `languageName.${lang === '' ? 'default' : lang}`,
            })}
          </option>
        ))
      }
    </FieldSelect>
  );
};

LanguageSelect.defaultProps = {
  optionToBlock: null,
};

LanguageSelect.propTypes = {
  optionToBlock: string,
  languages: arrayOf(string).isRequired,
  intl: intlShape.isRequired,
};

export default injectIntl(LanguageSelect);
