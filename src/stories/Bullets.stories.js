import React from 'react'
import Bullets, { BULLET_TYPES, CHECKMARK_COLORS, GRID_COUNT } from '../components/Bullets/Bullets'

export default {
  title: 'Basic/Bullets',
  component: Bullets,
  argTypes: {
    checkColor: {
      control: {
        type: 'radio',
        options: CHECKMARK_COLORS
      }
    },
    bulletType: {
      control: {
        type: 'radio',
        options: BULLET_TYPES
      }
    },
    gridCount: {
      control: {
        type: 'radio',
        options: GRID_COUNT.LIST
      }
    },
  },
};

export const Standard = (args) => <Bullets { ...args } />

Standard.args = {
  bullets: [
    'First bullet',
    'Second bullet'
  ],
  bulletType: 'regular',
  checkColor: 'black'
}

export const Inline = args => <Bullets { ...args } />

Inline.args = {
  bullets: [
    'Get access to 3 different coworking spaces',
    'No binding period & 7 day money-back guarantee',
    'Drop-in meeting rooms & all day coffee included'
  ],
  bulletType: BULLET_TYPES[1],
  checkColor: CHECKMARK_COLORS[1],
  displayAsGrid: true,
  gridCount: GRID_COUNT.THREE,
}
