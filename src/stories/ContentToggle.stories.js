import React, { useState } from 'react'
import { ContentToggle } from '../components'

export default {
  title: 'Basic/Content Toggle',
  component: ContentToggle
}

const Template = (args) => {
  const [openSection, updateOpenSection] = useState('subscriptions')
  const options = [
    { id: 'subscriptions', label: 'Subscriptions' },
    { id: 'onDemand', label: 'On demand' }
  ]

  return (
    <div>
      <ContentToggle { ...args } options={options} value={openSection} onChange={e => updateOpenSection(e)} />
      <h2>Open section: {openSection}</h2>
    </div>
  )
}

export const Standard = Template.bind({})

Standard.args = {
  name: 'contentToggle'
}
