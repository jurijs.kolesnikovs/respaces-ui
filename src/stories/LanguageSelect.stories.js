import React from 'react';
import { Form as FinalForm } from 'react-final-form';
import { LanguageSelect } from '../components';

export default {
  title: 'Composed/Language Select',
  component: LanguageSelect,
}

export const Standard = args => (
  <div style={{ maxWidth: '600px', margin: 'auto' }}>
    <FinalForm
      onSubmit={() => null}
      initialValues={{ languageSelect: 'en' }}
      render={({ values }) => {
        return (
          <>
            <p>{JSON.stringify(values)}</p>
            <LanguageSelect {...args} />
          </>
        )
      }}
    />
  </div>
);

Standard.args = {
  name: 'languageSelect',
  languages: [
    'en',
    'sv'
  ]
}
