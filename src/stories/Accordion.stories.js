import React from 'react';
import { Accordion } from '../components';

export default {
  title: 'Basic/Accordion',
  component: Accordion,
}

export const Standard = args => (
  <div style={{ width: '600px', margin: 'auto' }}>
    <Accordion { ...args } />
  </div>
)

Standard.args = {
  list: [
    {
      header: 'What are the requirements to be a Respaces host?',
      content: 'That you respect the Covid-19 regulations of the Swedish Health to keep your guests safe. Also, that you offer a quality workspace, coffee and fast WIFI.',
    },
    {
      header: 'How do I know how much I should charge?',
      content: 'Setting prices can be tricky. Look at the other listings on Respaces to get an idea och what you can charge. You can easily change the price later.' }
  ],
}
