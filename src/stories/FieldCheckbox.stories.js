import React from 'react';
import { Form as FinalForm } from 'react-final-form';
import { FieldCheckbox } from '../components';

export default {
  title: 'Basic/Field Checkbox',
  components: FieldCheckbox,
  argTypes: {
    useSuccessColor: {
      control: {
        type: 'boolean'
      }
    }
  }
};

export const Standard = args => (
  <div style={{ maxWidth: '400px', border: '1px solid teal' }}>
    <FinalForm
      onSubmit={() => null}
      initialValues={{ input: true }}
      render={({ values }) => (
        <>
          <p>Values: {JSON.stringify(values)}</p>
          <FieldCheckbox { ...args } />
        </>
      )}/>
  </div>
);

Standard.args = {
  id: 'input',
  name: 'input',
  label: 'Input label',
  useSuccessColor: false,
};
