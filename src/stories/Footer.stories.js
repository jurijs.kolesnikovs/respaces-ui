import React from 'react'

import Footer from '../components/Footer/Footer'

export default {
  title: 'Composed/Footer',
  component: Footer
}

export const Standard = (args) => (
  <div style={{ position: 'absolute', bottom: 0, width: '100%' }}>
    <Footer />
  </div>
)
