import React from 'react';
import { OrderConfirmationSummaryBox } from '../components';

export default {
  title: 'Composed/Order Confirmation Summary Box',
  component: OrderConfirmationSummaryBox,
};

export const Standard = args => <OrderConfirmationSummaryBox />
