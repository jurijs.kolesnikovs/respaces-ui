import React from 'react';
import { Form as FinalForm } from 'react-final-form';
import { FieldBirthdayInput } from '../components';

export default {
  title: 'Basic/Field Birthday Input',
  component: FieldBirthdayInput,
};

export const Standard = args => (
  <FinalForm
    onSubmit={() => null}
    render={({ values }) => {
      return (
        <>
          <p>{JSON.stringify(values)}</p>
          <FieldBirthdayInput {...args} />
        </>
      );
    }}
  />
);

Standard.args = {
  id: 'birthday',
  name: 'birthday',
  label: 'Birthday label'
}
