import React from 'react';
import { Title } from '../components';
import { PRIORITY } from '../components/Title/Title';

export default {
  title: 'Basic/Title',
  component: Title,
  argTypes: {
    priority: {
      control: {
        type: 'select',
        options: PRIORITY.LIST,
      }
    }
  }
};

export const Standard = args => <Title {...args}>Title</Title>;

Standard.args = {
  priority: PRIORITY.h1
}
