import React from 'react';

import { TransparentPricingCard } from '../components'

export default {
  title: 'Basic/Transparent Pricing Card',
  component: TransparentPricingCard
}

export const Standard = () => {
  return (
    <div style={{ maxWidth: '1090px', margin: '60px auto 40px', display: 'grid', gridTemplateColumns: 'repeat(3, 1fr)', gridColumnGap: '30px' }}>
      <TransparentPricingCard item={{ id: 1, days: 2, price: '1 500' }} />
      <TransparentPricingCard item={{ id: 2, days: 3, price: '2 200' }} isFavorite />
      <TransparentPricingCard item={{ id: 3, days: 4, price: '2 900' }} />
    </div>
  );
};
