import React from 'react';
import { Form as FinalForm } from 'react-final-form';
import { FieldTextInput } from '../components';

export default {
  title: 'Basic/Field Text Input',
  component: FieldTextInput,
}

export const Standard = args => (
  <div style={{ maxWidth: '600px', margin: 'auto' }}>
    <FinalForm
      onSubmit={() => null}
      initialValues={{
        testValue: ''
      }}
      render={({ values }) => {
        return (
          <>
            <p>{JSON.stringify(values)}</p>
            <FieldTextInput { ...args } />
          </>
        )
      }}
    />
  </div>
);

Standard.args = {
  id: 'testValue',
  name: 'testValue',
  type: 'text',
  label: 'Text input label',
  placeholder: 'Leave a feedback'
}
