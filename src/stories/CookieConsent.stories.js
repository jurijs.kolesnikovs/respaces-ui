import React from 'react';
import { CookieConsent } from '../components';

/* Clearing up cookies to display banner on reload */
if (typeof document !== 'undefined') {
  document.cookie = 'euCookiesAccepted=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

export default {
  title: 'Composed/Cookie Consent',
  component: CookieConsent
}

export const Standard = () => <CookieConsent />
