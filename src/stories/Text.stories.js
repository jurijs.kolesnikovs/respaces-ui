import React from 'react';
import { Text } from '../components';
import { SIZES, WEIGHTS } from '../components/Text/Text';

export default {
  title: 'Basic/Text',
  component: Text,
  argTypes: {
    size: {
      control: {
        type: 'radio',
        options: SIZES.LIST,
      },
    },
    weight: {
      control: {
        type: 'radio',
        options: WEIGHTS.LIST,
      },
    },
  },
};

export const Standard = args => <Text {...args}>Hello World</Text>;

Standard.args = {
  size: SIZES.MEDIUM,
  weight: WEIGHTS.BOLD,
  isUpperCased: false,
  isUnderlined: false,
};
