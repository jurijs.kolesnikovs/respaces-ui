import React from 'react';
import { MediaAndTextSection } from '../components';
import {
  TEXT_ALIGN,
  TEXT_ALIGN_MOBILE,
  BG_COLORS,
  SPACINGS,
  BULLET_TYPES,
  CHECKMARK_COLORS
} from '../components/MediaAndTextSection/MediaAndTextSection'
import { BUTTON_SIZES, BUTTON_TYPES, LINK_COLORS } from '../components/Button/Button'

export default {
  title: 'Composed/Media and text section',
  component: MediaAndTextSection,
  argTypes:{
    textAlign: {
      control: {
        type: 'radio',
        options: TEXT_ALIGN.LIST,
      }
    },
    textAlignMobile: {
      control: {
        type: 'radio',
        options: TEXT_ALIGN_MOBILE.LIST,
      }
    },
    bgColor: {
      control: {
        type: 'radio',
        options: BG_COLORS.LIST,
      }
    },
    spacing: {
      control: {
        type: 'radio',
        options: SPACINGS.LIST,
      }
    },
    bulletType: {
      control: {
        type: 'radio',
        options: BULLET_TYPES.LIST,
      }
    },
    checkColor: {
      control: {
        type: 'radio',
        options: CHECKMARK_COLORS.LIST,
      }
    }
  }
};

export const Standard = args => <MediaAndTextSection {...args} />

Standard.args = {
  title: {
    content: 'Two-sided section title',
    showMobile: true,
  },
  subtitle: {
    content: 'Subtitle',
    showMobile: true,
  },
  caption: {
    content: 'Caption',
    showMobile: true,
  },
  bullets: {
    list: [
      'First bullet',
      'Second bullet',
      'Third bullet'
    ],
    showMobile: true,
  },
  features: {
    list: [
      {
        title: 'First feature title',
        text: 'First feature text',
        showMobile: true,
      },
      {
        title: 'Second feature title',
        text: 'Second feature title',
        showMobile: true,
      },
      {
        title: 'Third feature title',
        text: 'Third feature text',
        showMobile: false,
      }
    ]
  },
  media: {
    content: 'https://source.unsplash.com/random/800x800',
    showMobile: true,
  },
  buttonProps: {
    withIcon: true,
    linkText: 'Get started now',
    to: '/inbox',
    type: BUTTON_TYPES.INLINE,
    size: BUTTON_SIZES.SMALL,
    linkColor: LINK_COLORS.BLUE,
  },
  textAlign: TEXT_ALIGN.LEFT,
  textAlignMobile: TEXT_ALIGN_MOBILE.BOTTOM,
  bgColor: BG_COLORS.LIGHT,
  spacing: SPACINGS.SMALL,
  bulletType: BULLET_TYPES.CHECK,
  checkColor: CHECKMARK_COLORS.GREEN,
  wideMedia: false,
}
