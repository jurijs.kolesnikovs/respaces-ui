import React, { useState } from 'react'
import ComboBox from 'components/ComboBox/ComboBox'

export default {
  title: 'Basic/ComboBox',
  component: ComboBox,
}

const Template = args => {
  const [isOpen, toggleOpen] = useState(false)
  const [value, updateValue] = useState('')
  return (
    <div style={{ maxWidth: '600px', margin: 'auto' }}>
      <p>Some other components to demonstrate overlay</p>
      <ComboBox {...args} value={value} isOpen={isOpen} onInput={e => updateValue(e)} onFocus={() => toggleOpen(true)} onClose={() => toggleOpen(false)}>
        <ul>
          <li onClick={() => {
            updateValue('Stockholm')
            console.log(`New value: ${value}`)
            toggleOpen(false)
          }}>Stockholm</li>
          <li onClick={() => {
            updateValue('Goteborg')
            console.log(`New value: ${value}`)
            toggleOpen(false)
          }}>Goteborg</li>
          <li onClick={() => {
            updateValue('Malmo')
            console.log(`New value: ${value}`)
            toggleOpen(false)
          }}>Malmo</li>
        </ul>
      </ComboBox>
      <p>Selected: {value}</p>
    </div>
  )
};

export const Standard = Template.bind({})
Standard.args = {
  borderType: 'left',
  placeholder: 'Location'
}
