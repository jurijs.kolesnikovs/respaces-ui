import React, { useState } from 'react';
import { CategoryComboBox } from '../components';

export default {
  title: 'Composed/Category ComboBox',
  component: CategoryComboBox,
};

export const Standard = args => {
  const [value, setValue] = useState('meeting');

  return (
    <div style={{ maxWidth: '600px', margin: 'auto' }}>
      <p>Value: {value}</p>
      <CategoryComboBox {...args} currentCategory={value} onChange={e => setValue(e)} />
    </div>
  )
}

Standard.args = {
  id: 'category',
  name: 'category',
  categories: [
    {
      key: 'coworking',
      label: 'Coworking',
      labelId: 'MarketplaceConfig.filters.category.coWorking'
    },
    {
      key: 'private',
      label: 'Office Space',
      labelId: 'MarketplaceConfig.filters.category.private'
    },
    {
      key: 'meeting',
      label: 'Meeting space',
      labelId: 'MarketplaceConfig.filters.category.meeting'
    },
    {
      key: 'studio',
      label: 'Studios',
      labelId: 'MarketplaceConfig.filters.category.studio'
    }
  ],
};
