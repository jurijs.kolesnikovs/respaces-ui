import React from 'react';
import Button, { BUTTON_TYPES, BUTTON_SIZES, LINK_COLORS } from 'components/Button/Button'

export default {
  title: 'Basic/Button',
  component: Button,
  argTypes: {
    type: {
      control: {
        type: 'radio',
        options: BUTTON_TYPES.LIST
      }
    },
    size: {
      control: {
        type: 'radio',
        options: BUTTON_SIZES.LIST
      }
    },
    linkColor: {
      control: {
        type: 'radio',
        options: LINK_COLORS.LIST
      }
    }
  }
}

export const Standard = (args) => (
  <div /*style={{ backgroundColor: '#eee', padding: '50px' }}*/>
    {args.type === 'white' && <p>Uncomment div styles to see button properly. Button used with dark background</p>}
    <Button {...args}>Button text</Button>
  </div>
);

Standard.args = {
  name: '/subscriptions',
  withIcon: false,
  type: BUTTON_TYPES.PRIMARY,
  size: BUTTON_SIZES.SMALL,
  linkColor: LINK_COLORS.BLACK,
  disabled: false
}

export const Link = (args) => <Button { ...args }>Blue link</Button>

Link.args = {
  name: '/subscriptions',
  withIcon: true,
  type: 'inline',
  size: BUTTON_SIZES.SMALL,
  linkColor: LINK_COLORS.BLUE,
}
