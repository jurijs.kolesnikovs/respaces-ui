import React from 'react'
import { ReviewCard } from '../components'

const reviews = [
  {
    id: 1,
    rating: 5,
    text: 'SectionHero.reviewText1',
    name: 'Cecilia',
  },
  {
    id: 2,
    rating: 5,
    text: 'SectionHero.reviewText2',
    name: 'Theodore',
  },
  {
    id: 3,
    rating: 5,
    text: 'SectionHero.reviewText3',
    name: 'Filippa',
  },
];

export default {
  title: 'Basic/Review Card'
};

export const Standard = () => (
  <div style={{ display: 'grid', gridTemplateColumns: 'repeat(3, 1fr)', gridColumnGap: '40px', maxWidth: '800px', marginTop: '45px', margin: 'auto' }}>
    {
      reviews.map(r => (
        <div>
          <ReviewCard key={r.id} review={r} />
        </div>
      ))
    }
  </div>
);
