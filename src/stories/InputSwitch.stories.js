import React from 'react';
import { Form as FinalForm } from 'react-final-form';

import { FieldSwitch } from '../components';
import { COLORS } from '../components/FieldSwitch/FieldSwitch';

export default {
  title: 'Basic/Input Switch',
  component: FieldSwitch,
  argTypes: {
    color: {
      control: {
        type: 'radio',
        options: COLORS.LIST,
      }
    }
  }
}

export const Standard = args => (
  <FinalForm
    onSubmit={() => null}
    initialValues={{
      [args.id]: false,
    }}
    render={({ values }) => (
      <>
        <p>Value: {JSON.stringify(values)}</p>
        <FieldSwitch {...args} isChecked={values[args.id]} />
      </>
    )}
  />
)

Standard.args = {
  id: 'open_menu_1',
  name: 'open_menu_1',
  color: COLORS.GREEN,
  withIcon: false,
  disabled: false,
  hideIconOnOff: false,
}
