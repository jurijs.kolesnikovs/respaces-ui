import React from 'react';
import { RatingStars } from '../components';
import { REVIEW_RATINGS, STAR_SIZES } from '../components/RatingStars/RatingStars';
import { FILL_COLORS } from '../components/IconReviewStar/IconReviewStar';

export default {
  title: 'Basic/Rating Stars',
  component: RatingStars,
  argTypes: {
    rating: {
      control: {
        type: 'radio',
        options: REVIEW_RATINGS,
      }
    },
    fillColor: {
      control: {
        type: 'radio',
        options: FILL_COLORS.LIST
      }
    },
    size: {
      control: {
        type: 'radio',
        options: STAR_SIZES.LIST
      }
    }
  }
};

export const Standard = args => <RatingStars { ...args } />

Standard.args = {
  rating: 4,
  fillColor: FILL_COLORS.GOLD,
};
