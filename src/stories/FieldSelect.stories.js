import React, { useState } from 'react';
import { Form as FinalForm } from 'react-final-form';
import { FieldSelect } from '../components';

export default {
  title: 'Basic/Field Select',
  component: FieldSelect,
}

export const Standard = args => {

  const options = [
    { key: 1, value: 'val_1', label: 'First' },
    { key: 2, value: 'val_2', label: 'Second' },
    { key: 3, value: 'val_3', label: 'Third' }
  ]

  return (
    <div style={{ maxWidth: '600px', margin: 'auto' }}>
      <FinalForm
        onSubmit={() => null}
        initialValues={{
          testValue: ''
        }}
        render={({ values }) => {
          return (
            <>
              <p>{JSON.stringify(values)}</p>
              <FieldSelect
                name="testValue"
                id="testValue"
                label="Test Time"
              >
                <option value="">Select the time</option>
                {options.map(opt => (
                  <option key={opt.key} value={opt.value}>{opt.label}</option>
                ))}
              </FieldSelect>
            </>
          )
        }}
      />
    </div>
  )
}
