import React from 'react';
import { LanguageSelector } from '../components';

export default {
  title: 'Basic/Language Selector',
  component: LanguageSelector
}

export const Standard = () => <LanguageSelector />
