import React from 'react';
import { AnimatedText } from '../components';

export default {
  title: 'Composed/Animated Text',
  component: AnimatedText,
};

export const Standard = () => <AnimatedText />
