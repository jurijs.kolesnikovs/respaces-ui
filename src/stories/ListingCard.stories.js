import React from 'react';
import { ListingCard } from '../components';
import { BADGE_COLORS } from '../components/ListingCard/ListingCard';

const firstImg = {
  attributes: {
    variants: {
      'square-small2x': {
        url: 'https://source.unsplash.com/random/430x270',
      },
    },
  },
};

const secondImg = {
  attributes: {
    variants: {
      'square-small2x': {
        url: 'https://source.unsplash.com/random/430x271',
      },
    },
  },
};

const thirdImg = {
  attributes: {
    variants: {
      'square-small2x': {
        url: 'https://source.unsplash.com/random/430x272',
      },
    },
  },
};

export default {
  title: 'Composed/Listing Card',
  component: ListingCard,
  argTypes: {
    badgeColor: {
      control: {
        type: 'radio',
        options: BADGE_COLORS.LIST,
      }
    }
  }
}

export const Standard = args => <ListingCard {...args} />

Standard.args = {
  id: '60ccc3a0-df85-48c3-bfbe-46e9c804c3b6',
  title: 'Date display in E-mail test',
  location: 'Hamngatan, Solna',
  type: 'Coworking',
  features: 'Desk, Drop-in meeting room',
  price: {
    formattedPrice: "5 000,00 kr",
    priceTitle: "5 000,00 kr"
  },
  rating: 4.75,
  isFavorite: false,
  images: [firstImg, secondImg, thirdImg],
  badgeText: 'Frukost ingår',
  badgeColor: BADGE_COLORS.GREEN,
  unitTranslationKey: 'UnitKey.perDay',
}
