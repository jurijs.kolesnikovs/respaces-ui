const canonicalRootURL = process.env.REACT_APP_CANONICAL_ROOT_URL || window.location.href;

const config = {
  canonicalRootURL,
};

export default config;
