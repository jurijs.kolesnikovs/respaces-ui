export const DAILY_BOOKING = 'daily';
export const HOURLY_BOOKING = 'hourly';
export const MONTHLY_BOOKING = 'monthly';

export const BOOKING_TYPES = [ DAILY_BOOKING, HOURLY_BOOKING, MONTHLY_BOOKING ];
