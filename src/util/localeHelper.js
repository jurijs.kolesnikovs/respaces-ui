import cookies from 'js-cookie';

export const getLocale = () => {
  if (typeof global !== 'undefined' && global.initLocale) {
    return global.initLocale;
  }

  if (cookies.get('locale')) {
    return cookies.get('locale') === 'sv' ? 'sv' : 'en';
  }

  return 'sv';
};

// function to change the website locale
export const setLocale = locale => {
  cookies.set('locale', locale, {
    path: '/',
  });
  window.location.reload();
};
