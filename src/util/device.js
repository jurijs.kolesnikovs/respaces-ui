function matchMedia(mediaQS) {
  if (typeof window !== 'undefined') {
    window.matchMedia = window.matchMedia
      // eslint-disable-next-line func-names
      || function () {
        return {
          matches: false,
        };
      };

    return window.matchMedia(mediaQS);
  }

  return {
    matches: false,
  };
}

export const narrowViewportMatch = () => matchMedia('(max-width: 878px)');

export const touchDeviceMatch = () => matchMedia('(pointer: coarse)');

export const isMobile = (narrowViewportMatch && narrowViewportMatch().matches)
  || (touchDeviceMatch && touchDeviceMatch().matches);
